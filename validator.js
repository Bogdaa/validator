const StringValidator = require('./validators/string');
const NumberValidator = require('./validators/number');
const BooleanValidator = require('./validators/boolean');
const DateValidator = require('./validators/date');
const ObjectValidator = require('./validators/object');
const ArrayValidator = require('./validators/array');

class Validator {
    static string() {
        return new StringValidator();
    }

    static number() {
        return new NumberValidator();
    }

    static boolean() {
        return new BooleanValidator();
    }

    static date() {
        return new DateValidator();
    }

    static object(objectToValidate) {
        return new ObjectValidator(objectToValidate);
    }

    static array() {
        return new ArrayValidator();
    }
}

const schema = Validator.array().length(5);

console.log(schema.validate(['aaaa']));





