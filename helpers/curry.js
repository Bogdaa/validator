const curry = (callback) => {
    return (...args) => {
        if (args.length >= callback.length) {
            return callback.apply(this, args);
        } else {
            return function(...args2) {
                return callback.apply(this, args.concat(args2));
            }
        }
    };
}

module.exports = curry;