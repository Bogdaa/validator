const { defaultTransformFn, AnyValidator } = require('./any')

const urlPattern = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/
const emailPattern = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/


const defaultValidateFn = (value) => {
    if(typeof value !== 'string') {
        return {
            error: true,
            message: 'Value is not a string'
        }
    }

    return value;
}

class StringValidator extends AnyValidator {
    constructor() {
        super(defaultValidateFn, defaultTransformFn);
        this.errorMessages = {
            ...super.getErrorMessages(),
            'string.alphanum': 'String contains other non-required symbols',
            'string.length': 'Length mismatch',
            'string.email': 'String must be valid email',
            'string.max': (length) => `String length must not be more than ${length} characters`,
            'string.min': (length) => `String length must not be less than ${length} characters`,
            'string.pattern': 'String must match the pattern',
            'string.token': 'String must be token'
        }
    }

    alphanum() {
        return this._applyValidator(
            (value) => {
                return value.match(/^[A-Za-z0-9]+$/)
            },
            'string.alphanum'
        )
    }

    length(length) {
        return this._applyValidator(
            (value) => value.length === length,
            'string.length'
        )
    }

    url() {
        return this._applyValidator(
            (value) => value.match(urlPattern),
            'string.pattern'
        )
    }

    email() {
        return this._applyValidator(
            (value) => value.match(emailPattern),
            'string.email'
        )
    }

    max(length) {
        return this._applyValidator(
            (value) => value.length <= length,
            'string.max',
            length,
        )
    }

    min(length) {
        return this._applyValidator(
            (value) => {
                return value.length >= length
            },
            'string.min',
            length
        )
    }

    pattern(regex) {
        return this._applyValidator(
            (value) => value.match(regex),
            'string.pattern'
        )
    }

    token() {
        return this._applyValidator(
            (value) => value.match(/^[A-Za-z0-9_]+$/),
            'string.token'
        )
    }

    uppercase() {
        return this._applyTransformer((value) => value.toUpperCase())
    }

    lowercase() {
        return this._applyTransformer((value) => value.toLowerCase())
    }

    trim() {
        return this._applyTransformer((value) => value.trim())
    }

    replace(pattern, term) {
        return this._applyTransformer((value) => value.replace(pattern, term))
    }
}

module.exports = StringValidator;