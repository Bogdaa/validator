const { defaultTransformFn, AnyValidator } = require('./any');

const defaultValidateFn = (value) => {
    if(typeof value !== 'boolean') {
        return {
            error: true,
            message: 'Value is not a boolean'
        }
    }

    return value;
}

class BooleanValidator extends AnyValidator {
    constructor() {
        super(defaultValidateFn,defaultTransformFn);
        this.errorMessages = {
            ...super.getErrorMessages(),
            'boolean.truthy': 'Value is not truthy',
            'boolean.falsy': 'Value is not falsy'
        }
    }

    truthy() {
        return this._applyValidator((value) => {
            return value === true
        }, 'boolean.truthy')
    }

    falsy() {
        return this._applyValidator((value) => {
            return value === false
        }, 'boolean.falsy')
    }
}

module.exports = BooleanValidator;
