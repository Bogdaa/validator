const compose = require('./../helpers/compose');
const { defaultTransformFn, AnyValidator } = require('./any');
const curry = require('../helpers/curry');

const defaultValidateFn = (value) => {
    if(typeof value !== 'object') {
        return {
            error: true,
            message: 'Value is not a object'
        }
    }

    return value;
}

const validateObject = curry(
    (schema, corteges) => corteges.map(([key, value]) => [key, schema[key].validate(value)])
);

class ObjectValidator extends AnyValidator {
    constructor(schema) {
        super(defaultValidateFn, defaultTransformFn);
        this.validate = compose(Object.fromEntries,validateObject(schema),Object.entries)
    }
}

module.exports = ObjectValidator;