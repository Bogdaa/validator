const compose = require('./../helpers/compose');

const wrapToValidateFn = (value) => value;
const defaultTransformFn = (value) => value

class AnyValidator {
    constructor(validateFn = wrapToValidateFn, transformFn = defaultTransformFn) {
        this._transform = transformFn;
        this.validate = (value) => compose(validateFn, this._transform)(value)
        this.errorMessages = {
            'any.allow': 'Unacceptable value',
            'any.custom': 'Custom validation doesn\'t pass'
        }
    }

    _applyTransformer(fn) {
        this._transform = compose(fn, this._transform);
        return this;
    }

    _applyValidator(fn, errorMessageCode, extraParam) {
        const newValidateFn = (value) => {
            if(value.error) {
                return value;
            }

            if(!fn(value)) {
                return {
                    error: true,
                    message: this._getErrorMessage(errorMessageCode, extraParam)
                }
            }
            return value;
        }
        this.validate = compose(newValidateFn, this.validate);
        return this;
    }

    _getErrorMessage(errorMessageCode, ...extraParams) {
        if(typeof this.errorMessages[errorMessageCode] === 'function') {
            return this.errorMessages[errorMessageCode](...extraParams)
        }

        return this.errorMessages[errorMessageCode]
    }

    getErrorMessages() {
        return this.errorMessages;
    }

    messages(messages) {
        this.errorMessages = {
            ...this.errorMessages,
            ...messages
        };
        return this;
    }

    allow(...values) {
        return this._applyValidator((value) => {
            return values.includes(value);
        }, 'any.allow')
    }

    custom(fn) {
        return this._applyValidator(fn, 'any.custom')
    }
}

module.exports = {
    AnyValidator,
    defaultTransformFn,
    wrapToValidateFn,
};