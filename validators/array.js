const { defaultTransformFn, AnyValidator } = require('./any');

const defaultValidateFn = (value) => {
    if(value instanceof Array) {
        return value;
    }

    return {
        error: true,
        message: 'Value is not an array'
    }
}

class ArrayValidator extends AnyValidator {
    constructor() {
        super(defaultValidateFn, defaultTransformFn);
        this.errorMessages = {
            ...super.getErrorMessages(),
            'array.max': (length) => `Array length must not be more than ${length} characters`,
            'array.min': (length) => `Array length must not be less than ${length} characters`,
            'array.items': 'Elements in array are not valid',
            'array.length': 'Array length mismatch'
        }
    }

    items(...items) {
        return this._applyValidator((value) => {
            return !value.some((item, index) => items[index].validate(item)?.error)
        }, 'array.items')
    }

    length(length) {
        return this._applyValidator((value) => {
            return value.length === length;
        },'array.length')
    }

    max(length) {
        return this._applyValidator(
            (value) => value.length <= length,
            'array.max',
            length,
        )
    }

    min(length) {
        return this._applyValidator(
            (value) => {
                return value.length >= length
            },
            'array.min',
            length
        )
    }
}

module.exports = ArrayValidator
