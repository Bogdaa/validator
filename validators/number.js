const { defaultTransformFn, AnyValidator } = require('./any');

const defaultValidateFn = (value) => {
    if(typeof value !== 'number') {
        return {
            error: true,
            message: 'Value is not a number'
        }
    }

    return value;
}

class NumberValidator extends AnyValidator {
    constructor() {
        super(defaultValidateFn, defaultTransformFn);
        this.errorMessages = {
            ...super.getErrorMessages(),
            'number.greater': (value) => `Number is greater than ${value}`,
            'number.less': (value) => `Number is less than ${value}`,
            'number.integer': 'Number is not a integer',
            'number.multiple': (value) => `Number must be base of ${value}`,
            'number.negative': 'Number is not negative',
            'number.positive': 'Number is not positive'
        }
    }

    greater(number) {
        return this._applyValidator((value) => {
            return number < value;
        }, 'number.greater', number)
    }

    less(number) {
        return this._applyValidator((value) => {
            return number > value;
        }, 'number.less', number)
    }

    integer() {
        return this._applyValidator((value) => {
            return Number.isInteger(value)
        }, 'number.integer')
    }

    multiple(base) {
        return this._applyValidator((value) => {
            return value % base === 0;
        }, 'number.multiple')
    }

    negative() {
        return this._applyValidator((value) => {
            return value < 0;
        }, 'number.negative')
    }

    positive() {
        return this._applyValidator((value) => {
            return value > 0;
        }, 'number.positive')
    }
}

module.exports = NumberValidator;