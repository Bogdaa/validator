const { defaultTransformFn, AnyValidator } = require('./any');

const defaultValidateFn = (value) => {
    if(value instanceof Date) {
        return value;
    }

    return {
        error: true,
        message: 'Value is not a valid date'
    }
}

class DateValidator extends AnyValidator {
    constructor() {
        super(defaultValidateFn, defaultTransformFn);
        this.errorMessages = {
            ...super.getErrorMessages(),
            'date.greater': (value) => `Date is less than ${value.toString()}`,
            'date.less': (value) => `Date is greater than ${value.toString()}`,
        }
    }

    greater(date) {
        return this._applyValidator((value) => {
            return value.getTime() > date.getTime()
        }, 'date.greater', date)
    }

    less(date) {
        return this._applyValidator((value) => {
            return value.getTime() < date.getTime()
        }, 'date.less', date)
    }
}

module.exports = DateValidator;